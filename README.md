Following Open source tools have been used for this project.
1.	Selenium 
2.	Java
3.	Eclipse IDE (Version: 2021-03 (4.19.0))


Steps to set up the project – 
1.	Install latest Jdk(Java development kit) version on the system from official Site.
2.	Download and install Latest Eclipse IDE
3.	Download and install Maven or Add maven plug-in to Eclipse
4.	Set system variables  for maven and java 
5.	Integrate TestNG with Eclipse. 
6.	Need to create Maven project with Latest and stable Selenium and TestNG dependencies from MVN official site.
7.	Keep Test code in Test/Java folder of project under package
8.	Keep initialization code in Main /Java folder.
9.  Keep dataDriven code in seperata class file under main/java folder. Create excel file and put login test data in it 
    and pass this file location in - FileInputStream fis=new FileInputStream("F:\\testdata-login.xlsx");
10.  Click on Run button on Eclipse from Home Page